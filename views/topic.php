<?php require_once ROOT . '/views/layouts/header.php' ?>

    <section class="section bgg">
        <div class="container">
            <div class="title-area">
                <h2><?php echo $topic['name'] ?></h2>
            </div><!-- /.pull-right -->
        </div><!-- end container -->
    </section>


    <!--<div class="container sitecontainer single-wrapper bgw">

        <div class="authorbox">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="post clearfix">
                        <div class="avatar-author">
                            <img alt="" src="/template/upload/avatar_02.png" class="img-responsive">
                        </div>
                        <div class="author-title desc">-->
<?php
$arr = array();
$arr = Comment::getParentComment();
foreach ($arr as $row) {
    TreeComment::tree($row);
}
?>


    <script type="text/javascript">
        $(document).ready(function () {
            $("a.reply").one('click', function () {
                var id = $(this).attr("id");
                var topic_id = $(this).attr("topic_id");

                $(this).parent().append("<div id='newform'><p><input type='text' name='author' id='author' placeholder='Your Name'/></p><p><input type='email' name='email' id='email' placeholder='Your Email'/></p><p><textarea name='text' id='text'></textarea></p><input type='hidden' name='parent_id' id='parent_id' value='" + id + "'/><input type='hidden' name='topic_id' id='topic_id' value='" + topic_id + "'/><p><button>Відправити</button></p></div>");
                $(function()
                {
                    $('textarea').autoResize();
                });
                $("button").click(function () {
                    var author = $("#author").val();
                    var text = $("#text").val();
                    var email = $("#email").val();
                    var parent_id = $("#parent_id").val();
                    var topic_id = $("#topic_id").val();

                    $.ajax({
                        url: "?action=addComment",
                        type: "POST",
                        data: {
                            author: author,
                            text: text,
                            email: email,
                            parent_id: parent_id,
                            topic_id: topic_id
                        },
                        success: function () {
                            $("#newform").prev().append("<div class='comment'><div class='container sitecontainer single-wrapper bgw'><div class='authorbox'><div class='text'>" + text + "</div></div></div></div>");
                            $("#newform").remove();
                        }
                    });
                });
            });

        });
    </script>
    <div id="new-comment"></div>
    <div class="container sitecontainer single-wrapper bgw">
        <div class="authorbox">


            <div class="form">
                <p>
                    <input type="text" name="author" id="author" placeholder="Your Name"/>
                </p>
                <p>
                    <input type="email" name="email" id="email" placeholder="Your Email"/>
                </p>
                <p>
                    <textarea name="text" id="text"></textarea>
                </p>
                <input type="hidden" name="parent_id" id="parent_id" value="0"/>
                <input type="hidden" name="topic_id" id="topic_id" value="<?= $topicId ?>"/>
                <p>
                    <button id="button">Відправити</button>
                </p>
            </div>
            <script type="text/javascript">
                $(document).ready(function () {
                    $("button").click(function () {
                        var author = $("#author").val();
                        var text = $("#text").val();
                        var email = $("#email").val();
                        var topic_id = $("#topic_id").val();
                        var parent_id = $("#parent_id").val();
                        $.ajax({
                            url: "?action=addComment",
                            type: "POST",
                            data: {
                                author: author,
                                text: text,
                                email: email,
                                parent_id: parent_id,
                                topic_id: topic_id
                            },
                            success: function () {
                                $("#new-comment").append("<div class='container sitecontainer single-wrapper bgw'><div class='authorbox'><div class='row'><div class='col-sm-12 col-md-12'><div class='post clearfix'><div class='avatar-author'><img src='/template/upload/avatar_02.png' class='img-responsive'></div><div class='author-title desc'><div class='author'>" + author + "</div><div class='email'>" + email + "</div><div class='text'>" + text + "</div></div></div></div></div></div></div>");
                                $("input#author").val("");
                                $("input#email").val("");
                                $("textarea#text").val("");
                            }
                        });
                    });

                });
            </script>
        </div>
    </div>
    <!--</div>-->
    <!--</div>-->
    <!--</div>--><!-- end col -->
    <!--</div>--><!-- end row -->
    <!--</div>--><!-- end authorbox -->
    <!--</div>--><!-- end container -->
<?php require_once ROOT . '/views/layouts/footer.php' ?>