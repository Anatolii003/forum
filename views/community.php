<?php require_once ROOT . '/views/layouts/header.php' ?>

    <div class="container sitecontainer bgw">
        <div class="row">
            <div class="col-md-12 m22 single-post">
                <div class="widget">
                    <div class="large-widget m30">
                        <div class="post-desc">
                            <div id="bbpress-forums">
                                <div class="table-responsive">
                                    <ul class="bbp-forums">
                                        <li class="bbp-header">
                                            <ul class="forum-titles">
                                                <li class="bbp-forum-info">Topics</li>
                                                <li class="bbp-forum-reply-count">Posts</li>
                                                <?php if ($user['admin'] == 1): ?>
                                                    <li class="bbp-forum-delete-topic">Delete topic</li>
                                                <?php endif; ?>
                                            </ul>
                                        </li><!-- .bbp-header -->
                                        <?php foreach ($topicList as $topic): ?>
                                            <?php $id = $topic['id']; ?>
                                            <?php $total = Comment::getTotalComments($id); ?>
                                            <li class="bbp-body">
                                                <ul class="forum type-forum status-publish hentry loop-item-0 odd bbp-forum-status-open bbp-forum-visibility-publish">
                                                    <li class="bbp-forum-info">
                                                        <a class="bbp-forum-title"
                                                           href="?action=view-topic&id=<?php echo $topic['id'] ?>"
                                                           title="General"><?php echo $topic['name'] ?></a>
                                                    </li>
                                                    <li class="bbp-forum-reply-count"><?php echo $total ?></li>

                                                    <?php if ($user['admin'] == 1): ?>
                                                        <li class="bbp-forum-delete-topic">
                                                            <a href="?action=delete-topic">Delete topic</a>
                                                        </li>
                                                    <?php endif; ?>

                                                </ul><!-- end bbp forums -->
                                            </li>
                                        <?php endforeach; ?>
                                    </ul><!-- .forums-directory -->
                                </div>
                            </div> <!-- /bbpress -->

                        </div>
                        <!-- end post-desc -->
                    </div>
                    <!-- end large-widget -->
                </div>
                <!-- end widget -->
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->


    <div class="container sitecontainer bgw">
        <div class="row">
            <div class="col-md-12 m22 single-post">
                <div class="widget">
                    <div class="large-widget m30">
                        <div class="post-desc">
                            <div id="bbpress-forums">
                                <div class="table-responsive">
                                    <ul class="bbp-forums">
                                        <li class="bbp-header">
                                            <ul class="forum-titles">
                                                <li class="bbp-forum-info">Categories</li>
                                                <li class="bbp-forum-topic-count">Topics</li>
                                                <?php if ($user['admin'] == 1): ?>
                                                    <li class="bbp-forum-delete-topic">Delete category</li>
                                                <?php endif; ?>
                                            </ul>
                                        </li><!-- .bbp-header -->
                                        <?php foreach ($categoryList as $category): ?>
                                            <?php $id = $category['id']; ?>
                                            <?php $total = Topic::getTotalTopics($id); ?>
                                            <li class="bbp-body">
                                                <ul class="forum type-forum status-publish hentry loop-item-0 odd bbp-forum-status-open bbp-forum-visibility-publish">
                                                    <li class="bbp-forum-info">
                                                        <a class="bbp-forum-title" href="#"
                                                           title="General"><?php echo $category['name'] ?></a>
                                                    </li>
                                                    <li class="bbp-forum-topic-count"><?php echo $total; ?></li>
                                                    <?php if ($user['admin'] == 1): ?>
                                                        <li class="bbp-forum-delete-topic">Delete category</li>
                                                    <?php endif; ?>
                                                </ul><!-- end bbp forums -->
                                            </li>
                                        <?php endforeach; ?>
                                    </ul><!-- .forums-directory -->
                                </div>
                            </div> <!-- /bbpress -->

                        </div>
                        <!-- end post-desc -->
                    </div>
                    <!-- end large-widget -->
                </div>
                <!-- end widget -->
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
<?php require_once ROOT . '/views/layouts/footer.php' ?>