<?php

class TreeComment
{

    public static function tree($row)
    {
        echo "<div class=\"container sitecontainer single-wrapper bgw\">
                <div class=\"authorbox\">
                    <div class=\"row\">
                        <div class=\"col-sm-12 col-md-12\">
                            <div class=\"post clearfix\">
                                <div class=\"avatar-author\">
                                    <img alt=\"\" src=\"/template/upload/avatar_02.png\" class=\"img-responsive\">
                                </div>
                                <div class=\"author-title desc\">";
        echo "<div class=\"author\">" . $row['author'] . "</div>";
        echo "<div class=\"email\">" . $row['email'] . "</div>";
        echo "<div class=\"text\">" . $row['text'] . "</div>";
        echo "<a href=\"#comment_from\" class=\"reply\" topic_id=\"" . $row['topic_id'] . "\" id=\"" . $row['id'] . "\">Відповісти</a>";
        echo "
                                </div>
                            </div>
                        </div><!-- end col -->
                    </div><!-- end row -->
                </div><!-- end authorbox -->
            </div><!-- end container -->";
        echo "<ul>";
        $son = array();
        $son = Comment::getChildComment($row);
        foreach ($son as $res) {
            self::tree($res);
        }
        echo "</ul>";

    }

}