<?php


function load($class){
    $arrayPath = array(
        '/components/',
        '/models/'
    );

    foreach($arrayPath as $path){
        $fileName = ROOT . $path . $class . '.php';
        if(file_exists($fileName)){
            include $fileName;
        }
    }
}

spl_autoload_register('load');