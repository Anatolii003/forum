<?php


class Router
{

    private $routes;

    public function __construct()
    {
        $arrayRoutes = ROOT . '/config/routes.php';
        $this->routes = include $arrayRoutes;
    }

    private function getUri()
    {
        if(isset($_SERVER['REQUEST_URI'])) {
            return ltrim($_SERVER['REQUEST_URI'], '/?action=');
        }
    }

    public function run()
    {
        $uri = $this->getUri();
        echo $uri;
        $segments = explode('&', $uri);
        echo '<pre>';
        print_r($segments);
        foreach($this->routes as $uriPattern => $path){

            if(preg_match("~$uriPattern~", $segments[0])){
                $replace = preg_replace("~$uriPattern~", $path, $uri);
                /*$segments = explode('/', $replace);*/

                $controllerName = ucfirst(array_shift($segments)) . 'Controller';

                $actionName = 'action' . ucfirst(array_shift($segments));


                $fileName = ROOT . '/controllers/' . $controllerName . '.php';
                if(file_exists($fileName)){
                    include $fileName;
                }

                $object = new $controllerName();
                $result = call_user_func_array(array($object, $actionName), $segments);

                if($result != null){
                    break;
                }

            }
        }
    }

}