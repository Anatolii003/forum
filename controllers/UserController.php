<?php


class UserController
{

    public function actionRegister()
    {

        $name = '';
        $email = '';
        $password = '';
        $result = false;

        if (isset($_POST['submit'])) {
            $name = $_POST['name'];
            $email = $_POST['email'];
            $password = $_POST['password'];


            $errors = array();

            if (User::checkName($name)) {

            } else $errors[] = 'Ім\'я повинно бути більше 3 символів';

            if (User::checkEmail($email)) {

            } else $errors[] = 'Невірний емейл';

            if (User::checkPassword($password)) {

            } else $errors[] = 'Пароль пвинен бути довше 6 символів';

            if (User::checkEmailExist($email)) {
                $errors[] = 'Такий email уже існує';
            }

            if (User::checkNameExist($name)) {
                $errors[] = 'Такий логін уже існує';
            }

            if (empty($errors)) {
                $result = User::register($name, $email, $password);
                /*var_dump($result);*/
            }
        }
        require_once ROOT . '/views/user/register.php';
        return true;
    }

    public function actionLogin()
    {
        $email = '';
        $password = '';

        if (isset($_POST['submit'])) {
            $email = $_POST['email'];
            $password = $_POST['password'];

            $errors = array();

            $user = User::checkUserData($email);


            if ($user == false) {
                $errors[] = 'Невірний email';
            } else {
                if (password_verify($password, $user['password'])) {
                    User::auth($user);
                    header('Location: /');
                } else {
                    $errors[] = 'Невірний пароль';
                }
            }
        }

        require_once(ROOT . '/views/user/login.php');

        return true;
    }

    public function actionLogout()
    {
        unset($_SESSION['user']);
        header("Location: /");
    }

}