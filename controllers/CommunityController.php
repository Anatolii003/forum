<?php


class CommunityController
{

    public function actionIndex()
    {

        $categoryList = array();
        $categoryList = Category::getCategoryList();

        $topicList = array();
        $topicList = Topic::getTopicsList();

        $user = User::checkLogged();

        require_once ROOT . '/views/community.php';
        return true;
    }

    public function actionTopic($id)
    {
        $commentsList = array();
        $commentsList = Comment::getCommentsList($id);

        $topic = Topic::getTopicById($id);

        $topicId = Comment::getTopicId();



        require_once ROOT . '/views/topic.php';
    }

    public function actionAddComment()
    {
        $author = $_POST['author'];
        $text = $_POST['text'];
        $parent_id = $_POST['parent_id'];
        $topic_id = $_POST['topic_id'];
        $date = date("Y-m-d H:i:s");
        $email = $_POST['email'];

        Comment::addComment($author, $text, $parent_id, $topic_id, $date, $email);
    }

}