<?php

session_start();
ini_set('display errors', 1);
error_reporting(E_ALL);

define('ROOT', dirname(__FILE__));
require_once ROOT . '/components/Autoload.php';

$action = isset($_GET['action']) ? $_GET['action'] : 'community';
switch($action){
    case 'community':
        require (ROOT . '/controllers/CommunityController.php');
        $object1 = new CommunityController();
        $object1->actionIndex();
        break;
    case 'view-topic':
        require (ROOT . '/controllers/CommunityController.php');
        $id = $_GET['id'];
        $object1 = new CommunityController();
        $object1->actionTopic($id);
        break;
    case 'login':
        require (ROOT . '/controllers/UserController.php');
        $object = new UserController();
        $object->actionLogin();
        break;
    case 'register':
        require (ROOT . '/controllers/UserController.php');
        $object = new UserController();
        $object->actionRegister();
        break;
    case 'logout':
        require (ROOT . '/controllers/UserController.php');
        $object = new UserController();
        $object->actionLogout();
        break;
    case 'addComment':
        require (ROOT . '/controllers/CommunityController.php');
        $object = new CommunityController();
        $object->actionAddComment();
        break;
    case 'addParentComment':
        require (ROOT . '/controllers/CommunityController.php');
        $object = new CommunityController();
        $object->actionAddParentComment();
        break;
}



