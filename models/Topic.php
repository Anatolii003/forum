<?php


class Topic
{

    public static function getTopicsList()
    {
        $db = Db::getConnection();

        $topicsList = array();

        $result = $db->query('SELECT id, category_id, date, name, author FROM topics ORDER BY id ASC LIMIT 10');

        $i = 0;
        while($row = $result->fetch()){
            $topicsList[$i]['id'] = $row['id'];
            $topicsList[$i]['name'] = $row['name'];
            $topicsList[$i]['category_id'] = $row['category_id'];
            $topicsList[$i]['date'] = $row['date'];
            $topicsList[$i]['author'] = $row['author'];
            $i++;
        }

        return $topicsList;
    }

    public static function getTopicById($id)
    {
        $id = intval($id);
        if($id) {
            $db = Db::getConnection();

            $result = $db->query('SELECT name FROM topics WHERE id = ' . $id);
            $result->setFetchMode(PDO::FETCH_ASSOC);
            return $result->fetch();
        }
    }

    public static function getTotalTopics($id)
    {
        $db = Db::getConnection();

        $result = $db->query('SELECT count(id) AS count FROM topics WHERE category_id = ' . $id);

        $row = $result->fetch();

        return $row['count'];
    }

}