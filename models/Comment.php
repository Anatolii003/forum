<?php

class Comment
{

    public static function getCommentsList($id)
    {
        $db = Db::getConnection();

        $commentsList = array();

        $result = $db->query('SELECT id, topic_id, date, text, author, email FROM comments WHERE parent_id = 0 AND topic_id = ' . $id . ' ORDER BY id ASC');

        $i = 0;
        while ($row = $result->fetch()) {
            $commentsList[$i]['id'] = $row['id'];
            $commentsList[$i]['topic_id'] = $row['topic_id'];
            $commentsList[$i]['date'] = $row['date'];
            $commentsList[$i]['text'] = $row['text'];
            $commentsList[$i]['author'] = $row['author'];
            $commentsList[$i]['email'] = $row['email'];
            $i++;
        }

        return $commentsList;
    }

    public static function getTotalComments($id)
    {
        $db = Db::getConnection();

        $result = $db->query('SELECT count(id) AS count FROM comments WHERE topic_id = ' . $id);

        $row = $result->fetch();

        return $row['count'];
    }

    public static function getTopicId()
    {
        $getTopicId = $_GET['id'];
        return $getTopicId;
    }

    public static function getParentComment()
    {
        $host = 'localhost';
        $dbname = 'forum';
        $user = 'root';
        $pass = '';

        $db = new PDO("mysql:host=$host; dbname=$dbname", $user, $pass);
        $array = array();
        $result = $db->query('SELECT * FROM comments WHERE parent_id = 0 AND topic_id = ' . self::getTopicId() . ' ORDER BY id ASC');

        $i = 0;
        while ($row = $result->fetch()) {
            $array[$i]['author'] = $row['author'];
            $array[$i]['email'] = $row['email'];
            $array[$i]['text'] = $row['text'];
            $array[$i]['id'] = $row['id'];
            $array[$i]['topic_id'] = $row['topic_id'];
            $i++;
        }
        return $array;
    }

    public static function getChildComment($row)
    {
        $host = 'localhost';
        $dbname = 'forum';
        $user = 'root';
        $pass = '';

        $db = new PDO("mysql:host=$host; dbname=$dbname", $user, $pass);
        $array = array();
        $result = $db->query('SELECT * FROM comments WHERE parent_id = ' . $row['id'] . ' AND topic_id = ' . self::getTopicId() . ' ORDER BY id ASC');
        if ($result != null) {
            $i = 0;
            while ($row = $result->fetch()) {
                $array[$i]['author'] = $row['author'];
                $array[$i]['email'] = $row['email'];
                $array[$i]['text'] = $row['text'];
                $array[$i]['id'] = $row['id'];
                $array[$i]['topic_id'] = $row['topic_id'];
                $i++;
            }
            return $array;
        }
    }

    public static function addComment($author, $text, $parent_id, $topic_id, $date, $email)
    {
        $db = Db::getConnection();

        $result = $db->prepare("INSERT INTO comments (author, text, parent_id, topic_id, date, email) VALUES (:author, :text, :parent_id, :topic_id, :date, :email)");

        $result->bindParam(':author', $author, PDO::PARAM_STR);
        $result->bindParam(':text', $text, PDO::PARAM_STR);
        $result->bindParam(':parent_id', $parent_id, PDO::PARAM_STR);
        $result->bindParam(':topic_id', $topic_id, PDO::PARAM_STR);
        $result->bindParam(':date', $date, PDO::PARAM_STR);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->execute();
        return $result;
    }



}